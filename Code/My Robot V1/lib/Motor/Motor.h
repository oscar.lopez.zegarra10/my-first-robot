#ifndef Motor_h
#include <Arduino.h>

#define Motor_h
#define PIN_MOTOR_RIGHT_FWD 18
#define PIN_MOTOR_RIGHT_BWD 19
#define PIN_RIGHT_MOTOR_SPEED 16
#define PIN_MOTOR_LEFT_FWD 5
#define PIN_MOTOR_LEFT_BWD 17
#define PIN_LEFT_MOTOR_SPEED 4

void Initialization(){

//Variables para la velocidad del motor
int globalMotorSpeed = 150;

//Inicializacion de los pines del motor
pinMode(PIN_MOTOR_RIGHT_FWD,OUTPUT);
pinMode(PIN_MOTOR_RIGHT_BWD,OUTPUT);
pinMode(PIN_MOTOR_LEFT_FWD,OUTPUT);
pinMode(PIN_MOTOR_LEFT_BWD,OUTPUT);
pinMode (PIN_RIGHT_MOTOR_SPEED,OUTPUT);
pinMode(PIN_LEFT_MOTOR_SPEED,OUTPUT);

//Configuramos los motores a velocidad de 150 hasta 255
analogWrite (PIN_RIGHT_MOTOR_SPEED,globalMotorSpeed);
analogWrite(PIN_LEFT_MOTOR_SPEED,globalMotorSpeed);

//Los motores se inician apagados
digitalWrite(PIN_MOTOR_RIGHT_FWD,LOW);
digitalWrite(PIN_MOTOR_RIGHT_BWD,LOW);
digitalWrite(PIN_MOTOR_LEFT_BWD,LOW);
digitalWrite(PIN_MOTOR_LEFT_FWD,LOW);


}

#endif