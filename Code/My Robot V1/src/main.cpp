/**
 * @file My firsr Robot .cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief My first stable robot with multiple basic functions
 * @version 1.0
 * @date 11.02.2023
 * 
 */


/***************************************************************************************************************
 *  									                       			INCLUDE
***************************************************************************************************************/

#include <Arduino.h>
#include <ESP32Servo.h>
#include "Ultrasonic.h" //Libreria creada para activar al sensor de ultrasonido
#include "Motor.h" 


using namespace std;


/***************************************************************************************************************
 *  						                  						  ENUMERATIONS
 **************************************************************************************************************/

enum Comands{
  //Comandos para el funcionamiento del carro 
  //CMD = "COMANDS" 
  CMD_FORWARD = 'w', 
  CMD_BACKWARD = 's', 
  CMD_RIGTH = 'a',
  CMD_LEFT = 'd', 
  CMD_STOP = 'q' 
};

/***************************************************************************************************************
 *  						                  						  GLOBAL VARIABLES
 **************************************************************************************************************/
Servo servo; //Nombre del servo motor
int globalPinServo = 2; //Variable del pin servo

//Variables para el control del sensor de ultrasionido
int globalDistance = 0;
int globalDelayValue = 600;
int globalObjectDetectionRange = 15;

//Variables para el control de la posicion del servo 
int globalServoInitialPosicion = 90;
int globalRighttAngleOfVision = 10;
int globalLeftAngleOfVision = 170;
int globalServoPosicion = 0;
int globalServoReadLeft = 0;
int globalServoReadRight = 0;


/***************************************************************************************************************
 *  					                							FUNCTION DECLARATION
 **************************************************************************************************************/
//Declaramos las funciones para luego utilizarlas
void setup();
void loop();
void PinInitialization(); //Inicializacion
void BytesReceived(); //Comandos del carro
void ObjectDetector(); //Detecta los objetos cercanos al carro
void StopCar(); //Detener el carro
void TurnForwardCar(); //Se mueve hacia adelante
void TurnBackwardCar(); //Se mueve hacia atras
void TurnRightCar(); //Se mueve hacia la derecha
void TurnLeftCar(); //Se mueve hacia la izquierda


/***************************************************************************************************************
 *  					                							FUNCTION DEFINITION
 **************************************************************************************************************/

void setup(){

PinInitialization();

//Activacion del monitor serial para poder colocar los comandos
Serial.begin(115200);

}
//=====================================================================================================

void loop(){
    
  if(Serial.available()> 0){  //Si hay comunicacion de datos 
    BytesReceived();
  }


}
//=====================================================================================================

void BytesReceived(){ // Despues de recibir el comando
  char bytesReceived = (char)Serial.read();
   
    switch(bytesReceived){

      //Va hacia adelante
      case CMD_FORWARD:
      Serial.println("Forward"); 
      TurnForwardCar();
      ObjectDetector();
      
      break;

      //Va hacia atras
      case CMD_BACKWARD:
      Serial.println("Backward");
      TurnBackwardCar();
      ObjectDetector();
      break;

      //Va hacia la derecha
      case CMD_RIGTH:
      Serial.println("Right");
      TurnRightCar();
      ObjectDetector();
      break;
    
      //Va hacia la izquierda
      case CMD_LEFT:
      Serial.println("Left");
      TurnLeftCar();
      ObjectDetector();
      break;

      //Se detiene el carro
      case CMD_STOP:
      Serial.println("Stop");
      StopCar();
      break;

      default: 
        break;
    }  
  }
//=====================================================================================================

void StopCar(){
  //Configuramos los motores para que se detenga
  digitalWrite(PIN_MOTOR_RIGHT_FWD,LOW);
  digitalWrite(PIN_MOTOR_RIGHT_BWD,LOW);
  digitalWrite(PIN_MOTOR_LEFT_FWD,LOW);
  digitalWrite(PIN_MOTOR_LEFT_BWD,LOW);
  
}
//=====================================================================================================

void TurnLeftCar(){
  //Configuramos los motores para que se dirija a la izquierda
  digitalWrite(PIN_MOTOR_RIGHT_FWD,HIGH);
  digitalWrite(PIN_MOTOR_RIGHT_BWD,LOW);
  digitalWrite(PIN_MOTOR_LEFT_FWD,LOW);
  digitalWrite(PIN_MOTOR_LEFT_BWD,HIGH);
  
}
//=====================================================================================================
void TurnRightCar(){
  //Configuramos los motores para que se dirija a la derecha
  digitalWrite(PIN_MOTOR_RIGHT_FWD,LOW);
  digitalWrite(PIN_MOTOR_RIGHT_BWD,HIGH);
  digitalWrite(PIN_MOTOR_LEFT_FWD,HIGH);
  digitalWrite(PIN_MOTOR_LEFT_BWD,LOW);
  
}
//=====================================================================================================

void TurnForwardCar(){
  //Configuramos los motores para que se dirija hacia adelante
  digitalWrite(PIN_MOTOR_RIGHT_FWD,HIGH);
  digitalWrite(PIN_MOTOR_RIGHT_BWD,LOW);
  digitalWrite(PIN_MOTOR_LEFT_FWD,HIGH);
  digitalWrite(PIN_MOTOR_LEFT_BWD,LOW);
  
}
//=====================================================================================================

void TurnBackwardCar(){
  digitalWrite(PIN_MOTOR_RIGHT_FWD,LOW);
  digitalWrite(PIN_MOTOR_RIGHT_BWD,HIGH);
  digitalWrite(PIN_MOTOR_LEFT_FWD,LOW);
  digitalWrite(PIN_MOTOR_LEFT_BWD,HIGH);
  
}
//=====================================================================================================

void ObjectDetector(){
  globalDistance = MeasuringDistance(); //Medir distancia
  Serial.println(globalDistance); // Muestra en el monitor serial la distancia

  //Se va a detener el carro de golpe si detecta un objeto cercano a los 15 centimetros
  if (globalDistance < globalObjectDetectionRange){
    StopCar();

    //El servo mirara a la derecha
    servo.write(globalRighttAngleOfVision);
    delay(globalDelayValue); //Un pequenio retraso para que pueda realizar sus procesos tranquilo
    globalServoReadRight = MeasuringDistance();

    //El servo mirara a la izquierda
    servo.write(globalLeftAngleOfVision);
    delay(globalDelayValue); //Un pequenio retraso para que pueda realizar sus procesos tranquilo
    globalServoReadLeft = MeasuringDistance();

    //En automatico el coche girara a la izquierda, esquivando el obstaculo
    if(globalServoReadLeft > globalServoReadRight){
      Serial.println("Giro hacia la izquierda");
      TurnLeftCar(); 
    }

    //En automatico el coche girara a la izquierda, esquivando el obstaculo
    if(globalServoReadRight > globalServoReadLeft){
      Serial.println("Giro hacia la derecha");
      TurnRightCar(); 
    }
  }
}
//=====================================================================================================

void PinInitialization(){

Initialization();

//Vinculamos el pin del servo indicando el rango de velocidad
servo.attach(globalPinServo,500,1500);

//Posicion del servo inicial
servo.write(globalServoInitialPosicion);

}